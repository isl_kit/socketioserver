# Socket.IO Server

This is a slim Node.js based [Socket.IO](https://socket.io/) server, which is responsible for sending real-time data to the clients (browser). It is intended to be used together with a webserver, that handles authentication, joins and leaves users to or from rooms and triggers the sending of data. An example for a webserver app, that connects to this Socket.IO server, can be found at [https://bitbucket.org/isl_kit/socketiodjangoapp.git](https://bitbucket.org/isl_kit/socketiodjangoapp.git).

## Install

    git clone https://bitbucket.org/isl_kit/socketioserver.git
    cd socketioserver
    npm install

## Usage

### Set configuration

Ensure that you have set all necessary environment variables or created a `.env` file, containing those values (see [Config](#config)).

### Start the server

    node server.js

For keeping this server running in a production environment, you can use the npm package [forever](https://www.npmjs.com/package/forever):

    npm install forever -g
    forever start server.js

## Events

There are only two events, that can be triggered by the client (browser) and are handled by the Socket.IO server. All other actions, like joining a room or sending data, are triggered by calling the API from the webserver.

### Connect

If a client establishes a connection, the Socket.IO server creates a Socket.IO session and notifies the webserver of this event. The webserver can then implement any kind of event handling, like sending initial data on connect.

### Disconnect

If a client disconnects, the Socket.IO session will be terminated and the webserver will also be notified for event handling purposes.


## API

For communication between the Socket.IO server and the webserver, the Socket.IO server exposes an API, that allows the webserver to trigger different actions between the Socket.IO server and the connected clients. The API is intended to be only callable from the webserver, NOT from the clients (browser) directly. This way, the webserver can perform necessary authentication before i.e. letting a user join a certain room.

Also, check the project [SocketioDjangoApp](https://bitbucket.org/isl_kit/socketiodjangoapp.git), which is a Django App, that runs on the webserver and makes use of this API.

### POST: /joined/

Inserts a Socket.IO client to a room.

    body: {
        sessionid: '48561838583',
        appid: 'myApp',
        room: 'roomXY'
    }

#### Params:

- sessionid: the ID of the Socket.IO session
- appid: a unique ID for the current app (is necessary if multiple apps use the same Socket.IO server instance)
- room: the name of the room, the client should be inserted to

### POST: /left/

Removes a Socket.IO client from a room.

    body: {
        sessionid: '48561838583',
        appid: 'myApp',
        room: 'roomXY'
    }

#### Params:

- sessionid: the ID of the Socket.IO session
- appid: a unique ID for the current app (is necessary if multiple apps use the same Socket.IO server instance)
- room: the name of the room, the client should be removed from

### POST: /send/

Sends a message to a single client.

    body: {
        sessionid: '48561838583',
        appid: 'myApp',
        message: {
            //....
        }
    }

#### Params:

- sessionid: the ID of the Socket.IO session
- appid: a unique ID for the current app (is necessary if multiple apps use the same Socket.IO server instance)
- message: the message to be sent to the client

### POST: /send_to/

Sends a message to a single client if that client is in a specified room.

    body: {
        sessionid: '48561838583',
        appid: 'myApp',
        room: 'roomXY',
        message: {
            //....
        }
    }

#### Params:

- sessionid: the ID of the Socket.IO session
- appid: a unique ID for the current app (is necessary if multiple apps use the same Socket.IO server instance)
- room: the name of the room, the client is supposed to be in
- message: the message to be sent to the client

### POST: /emit/

Broadcasts a message to all clients.

    body: {
        appid: 'myApp',
        message: {
            //....
        }
    }

#### Params:

- appid: a unique ID for the current app (is necessary if multiple apps use the same Socket.IO server instance)
- message: the message to be sent to the clients

### POST: /emit_to/

Broadcasts a message to all clients within a specified room.

    body: {
        appid: 'myApp',
        room: 'roomXY',
        message: {
            //....
        }
    }

#### Params:

- appid: a unique ID for the current app (is necessary if multiple apps use the same Socket.IO server instance)
- room: the room in which the message is broadcasted
- message: the message to be sent to the clients


## Config

Configuration is done with environment variables. Those variables can also be defined in a `.env` file, located in the directory from which `server.js` is called.

The following shows a list of all available settings.

### SOCKETIO_URL

The url under which the Socket.IO server should run:

    SOCKETIO_URL=http://localhost:4000

This parameter has to be in the format of `(http/https)://hostname:port`.

### SOCKETIO_CERT_FILE

This parameter is only required when using `https` protocol in the `SOCKETIO_URL` setting. It defines the path to the SSL certificate file:

    SOCKETIO_CERT_FILE=/path/to/cert.pem

### SOCKETIO_KEY_FILE

This parameter is only required when using `https` protocol in the `SOCKETIO_URL` setting. It defines the path to the SSL key file:

    SOCKETIO_KEY_FILE=/path/to/key.pem
    
## Additional Args


    --log_ip [optional: app_ids]
    
If argument is set, logs the hashed ips of all clients that connect when 'joined' API is called. 

If app_ids argument is left empty all hashes are written into 'log/all_ip.log'.

It is possible to log only the ips for specific apps by providing one or multiple app_ids (separated by comma).
    
    i.e. --log_ip lorem,ipsum
The hashes are written to 'log/${app_id}_ip.log'
