var url = require("url"),
    _ = require("underscore"),
    fs = require("fs"),
    http = require("http"),
    https = require("https"),
    querystring = require("querystring"),
    express = require("express"),
    bodyParser = require('body-parser'),
    crypto = require('crypto'),
    app = express(),
    server, io,
    protocol,
    auth_data = null,
    i, max_i, line,
    socketio_url, socketio_https, socketio_port, url_parts,
    ca, chain, cert,
    cert_file, key_file, chain_file,
    args = process.argv.slice(2),
    file_streams = {};
    blacklist = get_blacklist();
if (fs.existsSync('.env')) {
    require('dotenv').config();
}

// create file_streams if logging for ip is desired.
if (args[0] === '--log_ip') {
    if (!fs.existsSync('log')){
        fs.mkdirSync('log');
    }
    if (args[1]) {
        var rooms = args[1].split(",")
        for (var i = 0; i < rooms.length; i++) {
            file_streams[rooms[i]] = fs.createWriteStream("log/"+rooms[i]+"_ips.log",{flags:'a'});
        }
    } else {
        file_streams.allIPs = fs.createWriteStream("log/all_ips.log",{flags:'a'});
    }
}

function get_env_var(name) {
    var result = process.env[name];
    if (result === undefined) {
        throw "Environment variable " + name + " must be set!";
    }
    return result;
}

socketio_url = get_env_var("SOCKETIO_URL");
console.log(socketio_url);
url_parts = socketio_url.split(":");
socketio_https = url_parts[0] === "https";
socketio_port = parseInt(url_parts[2], 10);

if (socketio_https) {
    cert_file = get_env_var("SOCKETIO_CERT_FILE");
    key_file = get_env_var("SOCKETIO_KEY_FILE");
    server = https.createServer({
        key: fs.readFileSync(key_file),
        cert: fs.readFileSync(cert_file)
    }, app);
    protocol = https;
} else {
    server = http.createServer(app);
    protocol = http;
}

io = require("socket.io")(server);

server.listen(socketio_port);

app.use(bodyParser.json());

function getRoom(appId, room) {
    return room.length > 0 ? appId + "!!" + room : appId;
}

function getSessionId(body) {
    return "/#" + body.sessionid;
}

function findClientsSocket(roomId, namespace) {
    var res = [],
        ns = io.of(namespace || "/");    // the default namespace is "/"

    if (ns) {
        for (var id in ns.connected) {
            if(!roomId || ns.connected[id].rooms[roomId]) {
                res.push(ns.connected[id]);
            }
        }
    }
    return res;
}

app.post("/get_sockets_of_rooms/", function (req, res) {
    var body = req.body,
        rooms = body.rooms,
        result = {};

    _.each(rooms, function (r) {
        var room = getRoom(body.appid, r);
        result[r] = findClientsSocket(room).map(function(s) {
            return s.id;
        });
    });
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(result));
});

app.post("/joined/", function (req, res) {
    var body = req.body,
        socket = io.sockets.connected[getSessionId(body)],
        room = getRoom(body.appid, body.room);
    if (socket) {
        socket.join(room);
        if (args[0] === '--log_ip') {
	    var hashed_ip = crypto.createHash('md5').update(socket.handshake.headers['x-forwarded-for']).digest("hex")
            if (args[1]) {
                if (file_streams[body.appid]) {
                    file_streams[body.appid].write(hashed_ip +'\n');
                }
            } else {
                file_streams.allIPs.write(body.appid + " | " + hashed_ip +'\n')
            }
        }
    }
    res.send("joined: OK");
});

app.post("/left/", function (req, res) {
    var body = req.body,
        socket = io.sockets.connected[getSessionId(body)],
        room = getRoom(body.appid, body.room);
    if (socket) {
        socket.leave(room);
    }
    res.send("left: OK");
});

app.post("/send/", function (req, res) {
    var body = req.body,
        socket = io.sockets.connected[getSessionId(body)],
        room = getRoom(body.appid, ""),
        message = body.message;
    if (socket && _.contains(socket.rooms, room)) {
        socket.emit("message", message);
    }
    res.send("send: OK");
});

app.post("/send_to/", function (req, res) {
    var body = req.body,
        socket = io.sockets.connected[getSessionId(body)],
        room = getRoom(body.appid, body.room),
        message = body.message;
    if (socket && _.contains(socket.rooms, room)) {
        socket.emit("message_" + body.room, message);
    }
    res.send("send_to: OK");
});

app.post("/emit/", function (req, res) {
    var body = req.body,
        room = getRoom(body.appid, ""),
        message = body.message;
    io.to(room).emit("message", message);
    res.send("emit: OK");
});

app.post("/emit_to/", function (req, res) {
    var body = req.body,
        room = getRoom(body.appid, body.room),
        message = body.message;
    io.to(room).emit("message_" + body.room, message);
    res.send("emit_to: OK");
});

app.get("/get/", function (req, res) {
    res.send("HO");
});


function forward(type, data, sessionId, host, port, secure) {
    var options, req, protocol;

    data.sessionid = sessionId;
    data = JSON.stringify(data);
    protocol = secure ? https : http;

    options = {
        rejectUnauthorized: false,
        host: host,
        port: port,
        path: "/socket/" + type + "/",
        method: "POST",
        agent: false,
        headers: {
            "Content-Type": "application/json",
            "Content-Length": data.length
        }
    };

    //Send message to Django server
    try {
        req = protocol.request(options, function(res){
            res.setEncoding("utf8");
        });
        req.write(data);
        req.end();
    } catch (e) {
        console.log("Could not forward. Data: ");
        console.log(data);
        console.log("To:" + host + "/socket/" + type +":" + port);
        console.log(e);
    }
}

function get_blacklist() {
    if (fs.existsSync(".blacklist")) {
        var text = fs.readFileSync(".blacklist","utf-8");
        return text.split("\n");
    } else {
        return [];
    }
  
}


io.sockets.on("connection", function (socket) {
    var sessionId = socket.id, parsedUrl, secure;

     
    if (!socket.handshake.query.sender) {
        socket.disconnect(true);
        return;
    }
    if (blacklist.includes(socket.handshake.query.sender)) {
        return;
    }

    parsedUrl = url.parse(socket.handshake.query.sender);

    // Not sure what the reason is but sometimes empty connections are received on page load
    if (parsedUrl.protocol == null) {
        console.warn("Got empty connection. Discarding, but this might indicate a problem if it happens other than on page load.");
        return;
    }

    secure = parsedUrl.protocol.indexOf("https") === 0;

    forward("connect", {}, sessionId, parsedUrl.hostname, parsedUrl.port, secure);
    socket.on("disconnect", function () {
        forward("disconnect", {}, sessionId, parsedUrl.hostname, parsedUrl.port, secure);
    });
});

io.sockets.on("error", function(err) {
    console.log(err);
});
